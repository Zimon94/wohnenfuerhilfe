
class TemplateImporter {

    constructor(){
        this.allLoadedTemplates = [];
        this.templatePath = 'templates/';
        
    }

    getTemplate(fileName,callBackFunction = null){
        if(fileName in this.allLoadedTemplates){
            //console.log("template Loaded from templateImporter cache: " + fileName);
            if(callBackFunction != null){
                callBackFunction(this.allLoadedTemplates[fileName]);
            }
        }else{
            this.readTextFile(fileName,(callBackData)=>{
                
                this.allLoadedTemplates[fileName] = callBackData;
                if(callBackFunction != null){
                    callBackFunction(callBackData);
                }
            });
        }
    }

    readTextFile(fileName, callBack) {
        var rawFile = new XMLHttpRequest();
        rawFile.open("GET", this.templatePath + fileName, true);
        rawFile.onreadystatechange = function () {
            if (rawFile.readyState === 4) {
                if (rawFile.status === 200 || rawFile.status == 0) {
                    //console.log("template Loaded from File: " + fileName);
                    callBack(rawFile.responseText);
                }
            }
        }
        rawFile.send(null);
    }

    
}







