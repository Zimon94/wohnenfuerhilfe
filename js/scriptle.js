var connection = navigator.connection || navigator.mozConnection || navigator.webkitConnection;

function updateConnectionStatus() {
    window.location.reload();
}

connection.addEventListener('change', updateConnectionStatus);

function wiggle(){
    $('.notice').addClass('shake').delay(1000).removeClass('shake');
}