

class BurgerMenuLeft {
    constructor() {
        this.wrappingElement = document.getElementById("burgerMenuLeft");

        this.menuOpen = false;

        //prevent Scrolling on BurgerMenu
        this.wrappingElement.addEventListener('touchmove', function (e) {
            e.preventDefault();
        }, false);
    }

    updateMenuPoints(loggedIn) {
        if (loggedIn) {
            $('.loggedInMenuPoint').css('display', '');
            $('.loggedOutMenuPoint').css('display', 'none');
        } else {
            $('.loggedInMenuPoint').css('display', 'none');
            $('.loggedOutMenuPoint').css('display', '');
        }
    }

    close() {
        if (this.menuOpen) {
            this.menuOpen = false;
            application.menuBackGround.fadeToInvisible();
            this.demo = { score: 0 },

                this.tween = TweenLite.to(this.demo, 0.2, {
                    score: -80, ease: Power2.easeOut, onUpdate: () => {
                        this.wrappingElement.style.left = this.demo.score + '%';
                    }
                })
        }

    }

    open() {
        this.menuOpen = true;
        application.menuBackGround.fadeToBlack();
        this.demo = { score: -80 },

            this.tween = TweenLite.to(this.demo, 0.2, {
                score: 0, ease: Power2.easeOut, onUpdate: () => {
                    this.wrappingElement.style.left = this.demo.score + '%';
                }
            })
    }
}

class BurgerMenuRight {
    constructor() {
        this.wrappingElement = document.getElementById("burgerMenuRight");
        this.sendFormularButton = document.getElementById("suchMenuSuchButton");
        this.menuOpen = false;

        this.sendFormularButton.addEventListener('click', function (e) {
            application.navigateTo("alleAngebote")
            application.burgerMenuRight.close();
            let filter = {
                'ort': $('#searchOrt').val(),
                'umkreis': $('#searchUmkreis').val()
            }

            /*let filter = {
                'all': 1
            }*/
            application.alleAngeboteController.reloadWithFilters(filter, $('#searchType').val());


        })
        //prevent Scrolling on BurgerMenu
        this.wrappingElement.addEventListener('touchmove', function (e) {
            e.preventDefault();
        }, false);
    }

    close() {
        if (this.menuOpen) {
            this.menuOpen = false;
            application.menuBackGround.fadeToInvisible();
            this.demo = { score: 0 },

                this.tween = TweenLite.to(this.demo, 0.2, {
                    score: -80, ease: Power2.easeOut, onUpdate: () => {
                        this.wrappingElement.style.right = this.demo.score + '%';
                    }
                })
        }

    }

    open() {
        this.menuOpen = true;
        application.menuBackGround.fadeToBlack();
        this.demo = { score: -80 },

            this.tween = TweenLite.to(this.demo, 0.2, {
                score: 0, ease: Power2.easeOut, onUpdate: () => {
                    this.wrappingElement.style.right = this.demo.score + '%';
                }
            })
    }
}

class GreyMenuBackground {
    constructor() {
        this.maxOpacityValue = 0.3;
        this.backgroundElement = document.getElementById("transparentBackground");
        //prevent Scrolling on BurgerMenu
        this.backgroundElement.addEventListener('touchmove', function (e) {
            e.preventDefault();
        }, false);

        this.backgroundElement.addEventListener('click', function (e) {
            application.burgerMenuLeft.close();
            application.burgerMenuRight.close();
        })
    }

    fadeToBlack() {
        this.backgroundElement.style.pointerEvents = "auto"

        this.demo = { score: 0 },

            this.tween = TweenLite.to(this.demo, 0.2, {
                score: this.maxOpacityValue, ease: Power2.easeOut, onUpdate: () => {
                    this.backgroundElement.style.opacity = this.demo.score;
                }
            })
    }

    fadeToInvisible() {
        this.backgroundElement.style.pointerEvents = "none"

        this.demo = { score: this.maxOpacityValue },

            this.tween = TweenLite.to(this.demo, 0.2, {
                score: 0, ease: Power2.easeOut, onUpdate: () => {
                    this.backgroundElement.style.opacity = this.demo.score;
                }
            })
    }
}

