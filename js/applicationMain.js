

class ApplicationMain {

    constructor() {


        this.burgerMenuLeft = new BurgerMenuLeft();
        this.burgerMenuRight = new BurgerMenuRight();
        this.menuBackGround = new GreyMenuBackground();
        this.menuController = new MenuController();

        //Controller
        this.detailSeiteController = new DetailSeiteController();
        this.alleAngeboteController = new AlleAngeboteController();
        this.angebotErstellenController = new AngebotErstellenController();
        this.legeUserAnController = new LegeUserAnController();
        this.overlayHandler = new OverlayHandler('fullscreenOverlay');
        this.loginController = new LoginController();
        this.postfachController = new Postfach();
        this.loeschenController = new LoeschenController();
        this.schaetController = new SchaetController();

        this.templateImporter = new TemplateImporter();
        this.backendInterface = new BackendInterface();
        this.mainContentWrapper = document.getElementById('mainContent');

        this.currentStateID = "";
        this.previousStateID = "";
        this.currentStateController = null;
        this.currentUserID = null;
        this.navigateTo("alleAngebote");
        $('img').on("error", function () { $(this).attr('src', './uploads/missing.svg') });




    }

    navigateBack() {
        this.navigateTo(this.previousStateID);
    }

    navigateTo(stateID) {
        if (this.burgerMenuLeft.menuOpen) {
            this.burgerMenuLeft.close();
        }
        if (this.currentStateID == stateID) {
            return;
        }

        if (this.currentStateController != null) {
            this.currentStateController.deinit();
        }

        console.log("application.navigateTo: " + stateID);

        if (stateID == "alleAngebote") {
            this.simpleHTMLLoad("alleAngebote.html", () => {
                this.alleAngeboteController.init();
                this.currentStateController = this.alleAngeboteController;
            })

        } else if (stateID == "impressum") {
            this.simpleHTMLLoad('impressum.html', () => {
            })


        } else if (stateID == "alleGesuche") {
            var daten = 0;
            this.backendInterface.getAllHelpOffers((data) => {
                console.log(data);
                var dataneu = { 'inserat': data };
                this.createTemplateContent("gesuch.html", dataneu, (htmlString) => {
                    this.currentStateController = this.alleAngeboteController;
                });
            });


        } else if (stateID == "angebotErstellen") {
            this.simpleHTMLLoad("AngebotErstellenFormularAuswahl.html", () => {

                this.currentStateController = this.alleAngeboteController;

            });

        } else if (stateID == "jobErstellen") {
            this.loginController.verifyUser(() => {
                //OnSuccess
                this.simpleHTMLLoad("erstellejob.html", () => {
                    this.angebotErstellenController.initWohnraumanbieter();
                    this.currentStateController = this.alleAngeboteController;

                });
            }, () => {
                //OnFailure
                this.navigateTo("angebotErstellen");
            })


        } else if (stateID == "gesuchErstellen") {
            this.loginController.verifyUser(() => {
                //OnSuccess
                this.simpleHTMLLoad("gesuchErstellen.html", () => {
                    this.angebotErstellenController.initWohnraumsucher();
                    this.currentStateController = this.alleAngeboteController;

                });
            }, () => {
                //OnFailure
                this.navigateTo("angebotErstellen");
            })


        } else if (stateID == "impressum") {
            this.simpleHTMLLoad('impressum.html', () => {
                this.currentStateController = null;
            })
        }
        else if (stateID == "postfach") {
            this.loginController.verifyUser(() => {
                this.simpleHTMLLoad("postfach.html", () => {
                    $('.postfachContainer').html('<p id="statusField"><img class="loader" src="loader.gif"></p>');
                    this.postfachController.reload();
                    this.currentStateController = this.postfachController;
                })
            }, () => {
                //OnFailure
                this.navigateTo("angebotErstellen");
            })
        } else if (stateID.startsWith("messageDetailView_")) {
            this.showLoadingScreen();
            var neededID = stateID.split('_')[1];
            this.backendInterface.getChat(neededID, (data) => {
                var chatData = data;
                this.createTemplateContent("messageDetailView.html", data[0], () => {
                    this.schaetController.init(chatData);
                });
            })
            this.currentStateController = this.schaetController;


        } else if (stateID == "legeUserAn") {
            this.simpleHTMLLoad("legeUserAn.html", () => {
                this.legeUserAnController.init();
            });
        } else if (stateID == "login") {
            this.loginController.verifyUser(() => {
                //OnSuccess
                this.navigateTo('alleAngebote')
            }, () => {
                //OnFailure
                this.navigateTo("alleAngebote");
            })

        }
        else if (stateID == "eigenesProfil") {
            this.loginController.verifyUser(() => {
                //OnSuccess
                this.backendInterface.getSelfUserAndOfferdata((userDatawithoffers) => {
                    this.createTemplateContent('nutzerprofil.html', userDatawithoffers, (template) => {
                        this.currentStateController = this.loeschenController;
                    })
                })

            }, () => {
                //OnFailure
                this.navigateTo("alleAngebote");
            })

        } else if (stateID.startsWith("houseDetailView_")) {
            var neededID = stateID.split('_')[1];
            this.backendInterface.getHouseOfferByID(neededID, (jsonData) => {
                this.detailSeiteController.init(jsonData, 0);
                this.currentStateController = this.detailSeiteController;
            });


        } else if (stateID.startsWith("helpDetailView_")) {
            var neededID = stateID.split('_')[1];

            //HIER DIE FUNKTION FÜR DIE HELPDETAILVIEW @FLO
            this.backendInterface.getHelpOfferbyID(neededID, (jsonData) => {
                this.detailSeiteController.init(jsonData, 1);
                this.currentStateController = this.detailSeiteController;
            });


        } else {
            console.log("Application state ID not defined: " + stateID);
        }
        this.previousStateID = this.currentStateID;
        this.currentStateID = stateID;
    }


    refreshTemplate(currentState) {
        this.currentStateController.deinit();
        this.currentStateID = "refreshing";
        this.navigateTo(currentState);
    }

    createTemplateContent(templateFile, jsonData, callBackFunktion) {
        this.showLoadingScreen();
        this.getCompiledTemplate(templateFile, jsonData, (compiledTemplate) => {
            this.mainContentWrapper.innerHTML = compiledTemplate;
            $('img').on("error", function () { $(this).attr('src', './uploads/missing.png') });
            callBackFunktion(compiledTemplate);
        });
    }

    getCompiledTemplate(templateFile, jsonData, callBackFunktion) {

        this.templateImporter.getTemplate(templateFile, (templateString) => {
            let compiledTemplate = Template7.compile(templateString); //Compiliert das Template
            callBackFunktion(compiledTemplate(jsonData));
        });
    }

    simpleHTMLLoad(templateFile, callBackFunktion) {
        this.showLoadingScreen();
        this.templateImporter.getTemplate(templateFile, (templateString) => {
            this.mainContentWrapper.innerHTML = templateString;
            $('img').on("error", function () { $(this).attr('src', './uploads/missing.png') });
            callBackFunktion();
        });
    }

    showLoadingScreen() {
        this.mainContentWrapper.innerHTML = '<p id="statusField"><img class="loader" src="loader.gif"></p>';
    }
}