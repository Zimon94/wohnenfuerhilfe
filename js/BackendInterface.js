

class BackendInterface {

    constructor() {
        this.ajaxHandlerPath = "ajaxController.php";
        this.testData = {
            'request': {
                'offer': {
                    'house': {
                        'get': {
                            'id': '2'
                        }
                    }
                }
            }
        }
    }

    makeAsyncAjaxCall(postData, callBackFunction) {
        $.ajax({
            url: this.ajaxHandlerPath,
            data: postData,
            method: 'POST',
            async: false,
            success: function (data) {
                callBackFunction(data);
            }

        });
    }

    makeAjaxCall(postData, callBackFunction) {
        $.ajax({
            url: this.ajaxHandlerPath,
            data: postData,
            method: 'POST',
            async: true,
            success: function (data) {
                callBackFunction(data);
            }
        });
    }

    createUser(userData, callBackFunction) {
        var formData = {
            'request': {
                'user': {
                    'create': userData
                }
            }
        };
        console.log(formData);
        this.makeAjaxCall(formData, callBackFunction);
    }

    getHouseOffersIDs(callBackFunction) {
        var postData = {
            'request': {
                'offer': {
                    'house': {
                        'get': {
                            'filter': {
                                'all': 1,
                                'ajax': 1
                            }
                        }
                    }
                }
            }
        };
        this.makeAjaxCall(postData, callBackFunction);
    }

    createHouseOffer(jobdata, callbackfunction) {
        var postData = {
            'request': {
                'offer': {
                    'house': {
                        'create': jobdata
                    }
                }
            }
        };
        console.log(postData);
        this.makeAjaxCall(postData, callbackfunction);
    }

    createHelpOffer(jobdata, callbackFunction) {
        var postData = {
            'request': {
                'offer': {
                    'help': {
                        'create': jobdata
                    }
                }
            }
        };
        console.log(postData);
        this.makeAjaxCall(postData, callbackFunction);
    }

    deleteOffer(offertype, id, callbackFunction) {
        var postData
        switch (offertype) {
            case "angebot":
                postData = {
                    'request': {
                        'offer': {
                            'house': {
                                'delete': {
                                    'id': id
                                }
                            }
                        }
                    }
                };
                break;
            case "gesuch":
                postData = {
                    'request': {
                        'offer': {
                            'help': {
                                'delete': {
                                    'id': id
                                }
                            }
                        }
                    }
                };
                break;
        }
        this.makeAjaxCall(postData, callbackFunction);
    }

    getAllHelpOffers(callbackFunction) {
        var postData = {
            'request': {
                'offer': {
                    'help': {
                        'get': {
                            'filter': {
                                'all': '1'
                            }
                        }
                    }
                }
            }
        };
        this.makeAjaxCall(postData, callbackFunction);
    }

    getHelpOfferbyID(id, callbackFunction) {
        var postData = {
            'request': {
                'offer': {
                    'help': {
                        'get': {
                            'ids': {
                                0: id
                            }
                        }
                    }
                }
            }
        };
        this.makeAjaxCall(postData, callbackFunction);
    }

    getHelpOfferIDsWithFilter(filter, callBackFunction) {
        filter.ajax = 1;
        filter.all = 0;
        var postData = {
            'request': {
                'offer': {
                    'help': {
                        'get': {
                            'filter': filter
                        }
                    }
                }
            }
        };
        this.makeAjaxCall(postData, callBackFunction);
    }

    getHelpOffersByIDs(ids, callBackFunction) {
        var postData = {
            'request': {
                'offer': {
                    'help': {
                        'get': {
                            'ids': ids
                        }
                    }
                }
            }
        };

        this.makeAjaxCall(postData, callBackFunction);
    }

    getSelfUser(callBackFunction) {
        var postData = {
            'request': {
                'user': {
                    'get': {
                        'self': 1
                    }
                }
            }
        };
        this.makeAjaxCall(postData, callBackFunction);
    }
    getSelfUserAndOfferdata(callBackFunction) {
        var bietearray = [];
        var suchearray = [];
        this.getSelfUser((userData) => {
            var userOffer = userData;
            for (var i = 0; i < userData[0].angebot_biete.length; i++) {
                bietearray.push(userData[0].angebot_biete[i].id);
            }
            for (var i = 0; i < userData[0].angebot_suche.length; i++) {
                suchearray.push(userData[0].angebot_suche[i].id);
            }

            $.when(
                this.getHelpOffersAsync(suchearray, (jsonData) => {
                    userOffer[0].gesuche = jsonData;
                }),
                this.getHouseOffersAsync(bietearray, (jsonData) => {
                    userOffer[0].angebote = jsonData;
                })).done(function () {
                    var newUserOffer = { 'user': userOffer };
                    callBackFunction(newUserOffer);
                });

        })

    }

    getHouseOfferAndUserbyID(id, callbackFunction) {
        var offerData = {
            'request': {
                'offer': {
                    'house': {
                        'get': {
                            'ids': {
                                0: id
                            }
                        }
                    }
                }
            }
        };
        this.makeAjaxCall(offerData, (data) => {
            var offerdata = data;
            var userID = data[0].autor;
            var userData = {
                'request': {
                    'user': {
                        'get': {
                            'id': userID
                        }
                    }
                }
            };

            this.makeAjaxCall(userData, (data) => {
                offerdata[0].autorName = data[0].name;
                offerdata[0].autorNachname = data[0].nachname;
                offerdata[0].autorGeschlecht = data[0].geschlecht;
                offerdata[0].autorBeschreibung = data[0].beschreibung;
                offerdata[0].autorBild = data[0].bild;
                offerdata[0].autorMail = data[0].mail;
                callbackFunction(offerdata);
            })
        });
    }

    getHouseOffersIDsWithFilter(filter, callBackFunction) {
        filter.ajax = 1;
        filter.all = 0;
        var postData = {
            'request': {
                'offer': {
                    'house': {
                        'get': {
                            'filter': filter
                        }
                    }
                }
            }
        };
        this.makeAjaxCall(postData, callBackFunction);
    }

    getHouseOffers(ids, callBackFunction) {
        var postData = {
            'request': {
                'offer': {
                    'house': {
                        'get': {
                            'ids': ids
                        }
                    }
                }
            }
        };

        this.makeAjaxCall(postData, callBackFunction);
    }
    getHouseOffersAsync(ids, callBackFunction) {
        var postData = {
            'request': {
                'offer': {
                    'house': {
                        'get': {
                            'ids': ids
                        }
                    }
                }
            }
        };

        this.makeAsyncAjaxCall(postData, callBackFunction);
    }

    getHelpOffers(ids, callBackFunction) {
        var postData = {
            'request': {
                'offer': {
                    'help': {
                        'get': {
                            'ids': ids
                        }
                    }
                }
            }
        };

        this.makeAjaxCall(postData, callBackFunction);
    }
    getHelpOffersAsync(ids, callBackFunction) {
        var postData = {
            'request': {
                'offer': {
                    'help': {
                        'get': {
                            'ids': ids
                        }
                    }
                }
            }
        };

        this.makeAsyncAjaxCall(postData, callBackFunction);
    }

    getHouseOfferByID(id, callBackFunction) {
        var postData = {
            'request': {
                'offer': {
                    'house': {
                        'get': {
                            'ids': {
                                0: id
                            }
                        }
                    }
                }
            }
        };
        this.makeAjaxCall(postData, callBackFunction);
    }

    loginUser(mail, password, callBackFunction) {
        var postData = {
            'request': {
                'user': {
                    'login': {
                        'mail': mail,
                        'passwort': password
                    }
                }
            }
        }
        this.makeAjaxCall(postData, callBackFunction);
    }

    getOwnUser(callBackFunction) {
        var postData = {
            'request': {
                'user': {
                    'get': {
                        'self': 1
                    }
                }
            }
        }
        this.makeAjaxCall(postData, callBackFunction);
    }

    logoutUser(callBackFunction) {
        var postData = {
            'request': {
                'user': {
                    'logout': {
                        'logout': 1
                    }
                }
            }
        }
        this.makeAjaxCall(postData, callBackFunction);
    }

    getAllChats(callBackFunction) {
        var postData = {
            'request': {
                'chat': {
                    'get': {
                        'all': 1
                    }
                }
            }
        }
        this.makeAjaxCall(postData, callBackFunction);
    }

    getChat(id, callBackFunction) {
        var postData = {
            'request': {
                'chat': {
                    'get': {
                        'id': id
                    }
                }
            }
        }
        this.makeAjaxCall(postData, callBackFunction);
    }

    createChat(userID, angebotID, angebotHeadline, angebotType, callBackFunction) {
        var postData = {
            'request': {
                'chat': {
                    'create': {
                        'user_id': userID,
                        'angebot_id' : angebotID,
                        'angebot_headline': angebotHeadline,
                        'angebot_type' : angebotType

                    }
                }
            }
        }
        this.makeAjaxCall(postData, callBackFunction);
    }

    sendChat(chatID, text, callBackFunction) {
        var postData = {
            'request': {
                'chat': {
                    'send': {
                        'toChat': chatID,
                        'message': text
                    }
                }
            }
        }
        this.makeAjaxCall(postData, callBackFunction);
    }

}
