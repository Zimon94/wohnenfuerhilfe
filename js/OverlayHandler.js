

class OverlayHandler{

    constructor(wrapperID){
        this.overlayWrapper = document.getElementById(wrapperID);
        this.overlayWrapper.style.top = '100%';
    }

    loadSimpleHTMLintoOverlay(templateFile,callBackFunktion){
        application.templateImporter.getTemplate(templateFile, (templateString) => {
            this.overlayWrapper.innerHTML = templateString;
            callBackFunktion();
        });
    }

    showOverlay(){
        this.overlayWrapper.style.visibility = "visible"
        this.demo = { score: 120 },

                this.tween = TweenLite.to(this.demo, 0.3, {
                    score: 0, ease: Power2.easeOut, onUpdate: () => {
                        this.overlayWrapper.style.top = this.demo.score + '%';
                    }
                })
    }

    hideOverlay(){
        //this.overlayWrapper.style.visibility = "hidden"

        this.demo = { score: 0 },

                this.tween = TweenLite.to(this.demo, 0.3, {
                    score: 120, ease: Power2.easeOut, onUpdate: () => {
                        this.overlayWrapper.style.top = this.demo.score + '%';
                    }
                })
    }
}