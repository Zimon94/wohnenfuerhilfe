class Postfach {

    constructor() {
        this.interval = setInterval(this.reload, 10000);
        this.currentData = null;
        this.currentUnreadChats = 0;
    }

    init(template) {
        $('.postfachContainer').html(template);
    }

    reload() {
        if (application.loginController.isLoggedIn()) {
            application.backendInterface.getAllChats((data) => {

                if (data != "Es wurden keine Schäts gefunden.") {
                    if (this.currentData != data) {
                        this.currentUnreadChats = 0;

                        let jsonArray = { 'nachricht': data }
                        console.log(jsonArray)

                        for (let i = 0; i < jsonArray.nachricht.length; i++) {
                            
                            let bla = jsonArray.nachricht[i]
                            if(bla.messages[0] == undefined ){
                                console.log("undefined")
                                var nachrichtGelesen = true
                                var nochmalBla = "blabla";
                            }else{
                                var nochmalBla = bla.messages[0]
                                var nachrichtGelesen = (nochmalBla.gelesen == "1") || nochmalBla.von == "self";
                            }
                            
                            

                            

                            if (nochmalBla != null) {
                                if (!nachrichtGelesen) {
                                    this.currentUnreadChats++;
                                }

                                bla.chat.gelesen = nachrichtGelesen ? "display: none;" : ""
                                bla.chat.antiGelesen = !nachrichtGelesen ? "display: none;" : ""
                                //bla.chat.gelesen = nochmalBla.gelesen == "1";
                            } else {
                                bla.chat.antiGelesen = "";
                            }

                        }

                        jsonArray.nachricht.reverse()

                        application.getCompiledTemplate("nachrichtPreview.html", jsonArray, (compiledTemplate) => {

                            application.postfachController.init(compiledTemplate);
                        })
                        this.currentData = data;
                    }

                } else {
                    this.currentUnreadChats = 0;
                    $('.postfachContainer').html("Es wurden keine Anfragen gefunden");
                }

            //ALEX hier - diese Funktion wird alle 5 Sekunden aufgerufen, da kannst dann dein Element refreshen
            console.log(this.currentUnreadChats);
            if(this.currentUnreadChats > 0){
                $('.msgelement').addClass('unreadmsg');
                $('.msgelement').html(this.currentUnreadChats);
            }else{
                $('.msgelement').removeClass('unreadmsg');
                $('.msgelement').html("");
            }

            })
        }

    }

    createChat(userID, angebotID, angebotHeadline, angebotType) {
        var messageuserID = userID
        var messageangebotID = angebotID
        var messageangebotHeadline = angebotHeadline
        var messageangebotType = angebotType

        application.showLoadingScreen()
        application.loginController.verifyUser(() => {
            //OnSuccess
            application.backendInterface.createChat(messageuserID, messageangebotID, messageangebotHeadline, messageangebotType, (responseData) => {
                application.navigateTo("messageDetailView_" + responseData.chat_id)
            })
        }, () => {
            //OnFailure
            //this.navigateTo("alleAngebote");
        })
    }

    deinit() {

    }


}

/*

{{#each nachricht}}
<div class="nachrichtPreview" onclick="application.navigateTo('messageDetailView_{{chat.id}}')">
    <div  style="width:10px;"></div>
    <div style="{{this.chat.gelesen}}" class="gelesenIndikator" style="width:10px;"></div>
    <div class="adressatBild">
        <img src="./uploads/{{other.bild}}" alt="Profilbild">
    </div>
    <div class="nachrichtInfos">
            <p><strong style="{{chat.gelesen}}" > {{chat.angebot_headline}} <u>{{chat.angebot_headline}}</u> </strong><br> <i style="{{chat.antiGelesen}}">{{other.name}} {{other.nachname}}</i></p>
    </div>
    <div class="nachrichtenMenuPunkte">
        <img src="./icons/mehr%20optionen.svg">
    </div>
</div>
{{/each}}
*/