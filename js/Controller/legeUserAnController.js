var that = this;

class LegeUserAnController{

    initDropZone(paralleluploads){
        var bildPaths = [];
        this.bildPaths = bildPaths;
        Dropzone.autoDiscover = false;

        this.uploader = new Dropzone("#dropzoneFileUpload", {
            url: "dropzoneBackend.php",
            paramName: "file",
            uploadMultiple: true,
            acceptedFiles: "image/*",
            addRemoveLinks: false,
            forceFallback: false,
            maxFilesize: 1000,
            parallelUploads: paralleluploads,
            thumbnailHeight: 80,
            thumbnailWidth: 80
        });

        this.uploader.on("success", function (file, response) {
            bildPaths.push(response);
            this.bildPaths = bildPaths;
            console.log(bildPaths);
        });
    }

    init(){
        this.initvalidator();
        this.initDropZone(1);

    }
    initvalidator(){
        var self=this;
        var msgreq = "Dieses Feld ist ein Pflichtfeld. Bitte geben Sie einen Wert ein."
        $('form[id=userregistrierung]').validate({
            ignore: false,
            submitHandler: function(form, event) {
                event.preventDefault();
                var userdata = {
                    'name': $('input[id=name]').val(),
                    'nachname': $('input[id=nachname]').val(),
                    'geschlecht': $('select[id=geschlecht]').val(),
                    'beschreibung': $('.userbeschreibung').val(),
                    'mail': $('input[id=mail]').val(),
                    'bild': self.bildPaths[0],
                    'passwort': $('input[id=passwort]').val()
                };
                application.backendInterface.createUser(userdata, (responseText) => {
                    console.log(responseText);
                    alert("Sie wurden erfolgreich registriert!");
                    application.navigateTo("alleAngebote");
                });
                $("input").removeClass("falschesfeld");
                $("select").removeClass("falschesfeld");

            },
            invalidHandler: function(e,validator) {
                $("input").removeClass("falschesfeld");
                $("select").removeClass("falschesfeld");
                for (var i=0;i<validator.errorList.length;i++){
                    // "uncollapse" section containing invalid input/s:
                    $(validator.errorList[i].element).addClass("falschesfeld");
                }
            },
            errorClass: "alertmessage",
            validClass: "success text-success",
            rules: {
                name: {
                    maxlength: 15,
                    minlength: 2,
                    required:true,
                },
                nachname:{
                    maxlength: 15,
                    minlength: 2,
                    required:true,
                },
                mail:{
                    required:true,
                    email: true,
                    remote:{
                        url: 'ajaxController.php',
                        type: "post",
                        data:
                            {
                                'request':{
                                    'user':{
                                        'checkmail':{
                                            'mail': function()
                                            {
                                                return $('#mail').val();
                                            }
                                        }
                                    }
                                }
                            },
                        async: true

                    }
                },
                geschlecht:{
                    required:true,
                },
                passwort:{
                    required: true,
                    minlength: 8
                },
                pwrepeat:{
                    equalTo: '#passwort',
                    required: true
                },
                datenschutz:{
                    required:true
                }
            },
            messages:{
                name: {
                    maxlength: "Maximal 15 Zeichen sind erlaubt.",
                    minlength: "Bitte geben Sie mindestens 2 Zeichen an.",
                    required: msgreq,
                },
                nachname:{
                    maxlength: "Maximal 15 Zeichen sind erlaubt.",
                    minlength: "Bitte geben Sie mindestens 2 Zeichen an.",
                    required: msgreq,
                },
                mail:{
                    required: msgreq,
                    email: "Bitte geben Sie eine gültige E-Mail-Adresse an.",
                    remote: "Die E-Mail wird bereits für ein Nutzerkonto verwendet."
                },
                geschlecht:{
                    required: msgreq,
                },
                passwort:{
                    required: msgreq,
                    minlength: "Ihr Passwort muss mindestens 8 Zeichen enthalten."
                },
                pwrepeat:{
                    equalTo: 'Die eingegebenen Passwörter stimmen nicht überein.',
                    required: msgreq
                },
                datenschutz:{
                    required: 'Sie müssen unsere Datenschutzrichtlinien akzeptieren.'
                }
            }
        });
    }
}