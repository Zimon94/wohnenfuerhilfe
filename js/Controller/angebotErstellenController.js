console.log("Test");
var self = this;

class AngebotErstellenController {

    init() {

    }

    deinit(){

    }


    initWohnraumsucher(){
        this.initListener(1);
        this.initCollapseListener();
    }

    initWohnraumanbieter(){

        this.initListener(0);
        this.initCollapseListener();
        this.initDropZone(10);

    }

    initDropZone(paralleluploads){
        var bildPaths = [];
        this.bildPaths = bildPaths;
        Dropzone.autoDiscover = false;

        this.uploader = new Dropzone("#dropzoneFileUpload", {
            url: "dropzoneBackend.php",
            paramName: "file",
            uploadMultiple: true,
            acceptedFiles: "image/*",
            addRemoveLinks: false,
            forceFallback: false,
            maxFilesize: 1000,
            parallelUploads: paralleluploads,
            thumbnailHeight: 80,
            thumbnailWidth: 80
        });

        this.uploader.on("success", function (file, response) {
            bildPaths.push(response);
            this.bildPaths = bildPaths;
            console.log(bildPaths);
        });
    }

    getFormData(formtyp) {
        var aufgabenobjekt = [];
        $("input:checkbox[name=aufgaben]:checked").each(function(){
            aufgabenobjekt.push($(this).val());
        });
        switch(formtyp){
            case 0:
                var ausstattungsobjekt= [];

                $("input:checkbox[name=ausstattung]:checked").each(function(){
                    ausstattungsobjekt.push($(this).val());
                });

                $('form[id=dropzoneFileUpload]').submit(function(e){
                    e.preventDefault();
                });

                var formData = {
                    'bilder': this.bildPaths,
                    'titel': $('input[id=titel]').val(),
                    'groesse': $('input[id=groesse]').val(),
                    'preis': $('input[id=preis]').val(),
                    'mietart': $('select[id=mietart]').val(),
                    'von': $('input[id=datumvon]').val(),
                    'bis': $('input[id=datumbis]').val(),
                    'ausstattung': ausstattungsobjekt,
                    'aufgabenfeld': aufgabenobjekt,
                    'sonstiges': $('#sonstiges').val(),
                    'arbeitsstunden': $('input[id=arbeitsstunden]').val(),
                    'plz': $('input[id=postleitzahl]').val(),
                    'ort': $('input[id=ort]').val(),
                    'adresse': $('input[id=adresse]').val(),
                    'wohnungsart': $('select[id=wohnungsart]').val(),
                    'lage': $('select[id=lage]').val(),
                    'hauptbild': this.bildPaths[0]
                };

                break;
            case 1:
                var formData = {
                    'titel': $('input[id=titel]').val(),
                    'preis': $('input[id=preis]').val(),
                    'mietart': $('select[id=mietart]').val(),
                    'von': $('input[id=datumvon]').val(),
                    'bis': $('input[id=datumbis]').val(),
                    'sonstiges': $('#sonstiges').val(),
                    'arbeitsstunden': $('input[id=arbeitsstunden]').val(),
                    'lage': $('select[id=lage]').val(),
                    'plz': $('input[id=postleitzahl]').val(),
                    'ort': $('input[id=ort]').val(),
                    'bilder': '',
                    'ausstattung': '',
                    'aufgabenfeld': aufgabenobjekt,
                    'umkreis' : $('input[id=umkreis]').val()
                }
                break;
        };
        return formData;


    }

    sendData(jobdaten,formtyp) {
        switch(formtyp){
            case 0:
                application.backendInterface.createHouseOffer(jobdaten, (responseText) => {
                    console.log(responseText);
                    alert("Angebot erstellt!");
                    application.navigateTo("alleAngebote");
                });
                break;
            case 1:
                application.backendInterface.createHelpOffer(jobdaten, (responseText) => {
                    console.log(responseText);
                    alert("Gesuch erstellt!");
                    application.navigateTo("alleAngebote");
                });
                break;
        }
        console.log(jobdaten);

    }

    initListener(formtyp) {
        var self = this;
        var msgreq = "Dieses Feld ist ein Pflichtfeld. Bitte geben Sie einen Wert ein.";

        switch(formtyp){
            case 0: //JOB
                $('form[id=angebotFormular]').validate({
                    ignore: false,
                    submitHandler: function(form, event) {
                        // do other things for a valid form
                        var jobdaten = self.getFormData(formtyp);
                        console.log(jobdaten);
                        self.sendData(jobdaten, formtyp);
                        event.preventDefault();
                        $("input").removeClass("falschesfeld");
                        $("select").removeClass("falschesfeld");

                    },
                    invalidHandler: function(e,validator) {
                        $(".collapse").collapse('show');
                        $("input").removeClass("falschesfeld");
                        $("select").removeClass("falschesfeld");
                        for (var i=0;i<validator.errorList.length;i++){
                            // "uncollapse" section containing invalid input/s:
                            $(validator.errorList[i].element).addClass("falschesfeld");
                        }
                    },
                    errorClass: "alertmessage",
                    validClass: "success text-success",
                    rules: {
                        // The key name on the left side is the name attribute
                        // of an input field. Validation rules are defined
                        // on the right side
                        titel: {
                            required:true,
                            minlength: 5,
                        },
                        groesse: {
                            required: true,
                            digits: true,
                        },
                        preis: {
                            required: true,
                        },
                        datumvon:{
                            required: true,
                        },
                        datumbis: {},
                        aufgabenfeld:{
                            required: true,
                        },
                        sonstiges:{},
                        arbeitsstunden:{
                            required: true,
                            max: 80,
                            min: 0,
                        },
                        wohnungsart:{},
                        lage: {
                            required: true,
                        },
                        ort:{
                            required: true,
                        },
                        postleitzahl:{
                            required: true,
                        }
                    },
                    messages:{
                        titel: {
                            required: msgreq,
                            minlength: "Bitte geben Sie mehr als 5 Zeichen an."
                        },
                        groesse: {
                            required: msgreq,
                            digits: "Bitte nur ganze Zahlen angeben."
                        },
                        preis: {
                            required: msgreq
                        },
                        mietart: {
                            required: msgreq,
                        },
                        aufgabenfeld: {
                            required: msgreq
                        },
                        arbeitsstunden: {
                            required: msgreq,
                            max: "Mehr als 80 Stunden pro Woche sind nicht zulässig."
                        },
                        lage: {
                            required: msgreq
                        },
                        datumvon:{
                            required: msgreq
                        },
                        ort: {
                            required: msgreq
                        },
                        postleitzahl: {
                            required: msgreq
                        }
                    }
                });
                break;
            case 1: //HILFE
                $('form[id=gesuchFormular]').validate({
                    ignore: false,
                    submitHandler: function(form, event) {
                        // do other things for a valid form
                        var jobdaten = self.getFormData(formtyp);
                        console.log(jobdaten);
                        self.sendData(jobdaten, formtyp);
                        event.preventDefault();
                        $("input").removeClass("falschesfeld");
                        $("select").removeClass("falschesfeld");

                    },
                    invalidHandler: function(e,validator) {
                        $(".collapse").collapse('show');
                        $("input").removeClass("falschesfeld");
                        $("select").removeClass("falschesfeld");
                        for (var i=0;i<validator.errorList.length;i++){
                            // "uncollapse" section containing invalid input/s:
                            $(validator.errorList[i].element).addClass("falschesfeld");
                        }
                    },
                    errorClass: "alertmessage",
                    validClass: "success text-success",
                    rules: {
                        titel: {
                            required:true,
                            minlength: 5,
                        },
                        preis: {
                            required: true,
                        },
                        mietart: {
                            required: true,
                        },
                        datumvon:{
                            required: true,
                        },
                        datumbis: {},
                        sonstiges:{},
                        arbeitsstunden:{
                            required: true,
                        },
                        ort:{
                            required: true,
                        },
                        postleitzahl:{
                            required: true,
                        },
                        umkreis: {
                            required:true
                        }
                    },
                    messages:{
                        titel: {
                            required: msgreq,
                            minlength: 5,
                        },
                        preis: {
                            required: msgreq,
                        },
                        mietart: {
                            required: msgreq,
                        },
                        datumvon:{
                            required: msgreq,
                        },
                        datumbis: {},
                        sonstiges:{},
                        arbeitsstunden:{
                            required: msgreq,
                        },
                        ort:{
                            required: msgreq,
                        },
                        postleitzahl:{
                            required: msgreq,
                        },
                        umkreis:{
                            required: msgreq
                        }
                    }
                });
        }
    }

    initCollapseListener(){
        $( ".slidebutton" ).click(function() {
            $(this).find('.slideicon').toggleClass('glyphicon-chevron-down');
            $(this).find('.slideicon').toggleClass('glyphicon-chevron-up');
        });
    }
}
