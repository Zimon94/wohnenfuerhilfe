var currentChatData = null;

class SchaetController {


    init(data) {
        this.data = data[0];
        currentChatData = this.data;
        console.log(this.data)
        this.messagesContainerID = "messageWrapper";
        this.scrollToBottom();
        this.initTextBoxResizer();
        this.reloadMessages();
        this.interval = setInterval(this.reloadMessages, 2000);
    }

    reloadMessages() {
        //var objAssetSelection = $.parseJSON(strAssetSelection);
        //objAssetSelection.info.reverse();
        application.backendInterface.getChat(currentChatData.chat.id, (data) => {

            
            if (currentChatData != data[0]) {
                let messages = data[0];
                messages.messages.reverse();

                for (let i = 0; i < messages.messages.length; i++) {
                    messages.messages[i].datum = timeConverter(messages.messages[i].datum);
                }

                application.getCompiledTemplate('message.html', messages, (htmlData) => {
                    $('#messageWrapper').html(htmlData);
                    application.schaetController.scrollToBottom();
                })
            }

        })
    }

    deinit() {
        if (this.interval != null) {
            clearInterval(this.interval);
        }
    }

    sendMessage() {
        let message = $('#messageInputField').val();
        $('#messageInputField').val('')
        application.backendInterface.sendChat(this.data.chat.id, message, (responseData) => {
            if (responseData == "Nachricht wurde versendet.") {
                this.reloadMessages();
            }
        })



        console.log(message)
    }

    scrollToBottom() {
        window.scrollTo(0, document.body.scrollHeight || document.documentElement.scrollHeight);

    }

    initTextBoxResizer() {
        var observe;
        if (window.attachEvent) {
            observe = function (element, event, handler) {
                element.attachEvent('on' + event, handler);
            };
        }
        else {
            observe = function (element, event, handler) {
                element.addEventListener(event, handler, false);
            };
        }

        var text = document.getElementById('messageInputField');
        function resize() {
            text.style.height = 'auto';
            text.style.height = text.scrollHeight + 'px';
        }
        /* 0-timeout to get the already changed text */
        function delayedResize() {
            window.setTimeout(resize, 0);
        }
        observe(text, 'change', resize);
        observe(text, 'cut', delayedResize);
        observe(text, 'paste', delayedResize);
        observe(text, 'drop', delayedResize);
        observe(text, 'keydown', delayedResize);

        text.focus();
        text.select();
        resize();

    }
}