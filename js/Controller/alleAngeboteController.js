
class AlleAngeboteController {

    constructor() {
        this.angebotContainerID = 'angeboteContainer';
        this.statusFieldID = 'statusField';
        this.offersPerPage = 5;
        this.templateName = 'angebot.html';
        this.helpTemplateName = 'gesuch.html';
        this.active = false;
        this.currentFilter = null;
        this.currentType = "house";
    }

    init() {
        this.currentFilter = null;
        this.active = true;
        this.inViewPortListenerElement1 = null;
        this.inViewPortListenerElement2 = null;
        this.notYetLoadedIDs = null;
        this.angebotContainer = document.getElementById(this.angebotContainerID);
        this.statusFieldContainer = document.getElementById(this.statusFieldID);
        application.templateImporter.getTemplate(this.templateName, () => {
            this.updateAngebote(() => {
                this.appendCards();
                this.activateScrollListener(true);
            });
        });
    }

    deinit() {
        this.activateScrollListener(false);
    }





    reloadWithFilters(filter, type) {

        this.currentType = type;
        this.currentFilter = filter;
        console.log(this.currentType);
        this.angebotContainer.innerHTML = "";
        // this.showMessage('loading');
        this.loading();
        this.updateAngebote(() => {
            this.appendCards();
        })
    }

    activateScrollListener(listenerOn) {
        var selfAngebotController = this;
        if (listenerOn) {
            window.addEventListener("scroll", this.OnScrollEvent)
        } else {
            window.removeEventListener("scroll", this.OnScrollEvent)
        }
    }

    OnScrollEvent() {
        if (application.alleAngeboteController.checkIfReloadNecessary()) {
            application.alleAngeboteController.activateScrollListener(false);
            application.alleAngeboteController.appendCards();
        }
    }

    updateAngebote(callBackFunction) {
        if (this.currentFilter == null) {
            application.backendInterface.getHouseOffersIDs((data) => {
                this.notYetLoadedIDs = data.reverse();
                callBackFunction();
            })
        } else {
            if (this.currentType == "house" || this.currentType == null) {
                application.backendInterface.getHouseOffersIDsWithFilter(this.currentFilter, (data) => {
                    this.notYetLoadedIDs = data.reverse();
                    callBackFunction();
                })
            } else {
                application.backendInterface.getHelpOfferIDsWithFilter(this.currentFilter, (data) => {
                    console.log(data)
                    this.notYetLoadedIDs = data.reverse();
                    callBackFunction();
                })
            }
        }
    }

    appendCards() {
        if (this.notYetLoadedIDs.length != 0) {
            this.loading();
            // this.showMessage("loading...");
            var neededIDs = this.notYetLoadedIDs.slice(0, this.offersPerPage);
            this.notYetLoadedIDs.splice(0, this.offersPerPage);
            var tempArray = {}
            for (let index = 0; index < neededIDs.length; index++) {
                tempArray[index] = neededIDs[index].id;
            }

            if (this.currentType == "house") {
                application.backendInterface.getHouseOffers(tempArray, (data) => {
                    data = { 'inserate': data }
                    application.getCompiledTemplate(this.templateName, data, (htmlString) => {
                        this.angebotContainer.innerHTML += htmlString;
                        $('img').on("error", function () { $(this).attr('src', './uploads/missing.png') });
                        if (neededIDs[neededIDs.length - 2] != null) {
                            this.inViewPortListenerElement1 = document.getElementById('viewPortCheck_' + neededIDs[neededIDs.length - 2].id)
                        }
                        this.inViewPortListenerElement2 = document.getElementById('viewPortCheck_' + neededIDs[neededIDs.length - 1].id)
                        this.activateScrollListener(true);
                        this.showMessage("");
                    })
                })
            } else {
                application.backendInterface.getHelpOffersByIDs(tempArray, (data) => {
                    data = { 'inserat': data }
                    application.getCompiledTemplate(this.helpTemplateName, data, (htmlString) => {
                        this.angebotContainer.innerHTML += htmlString;
                        $('img').on("error", function () { $(this).attr('src', './uploads/missing.png') });
                        if (neededIDs[neededIDs.length - 2] != null) {
                            this.inViewPortListenerElement1 = document.getElementById('viewPortCheck_' + neededIDs[neededIDs.length - 2].id)
                        }
                        this.inViewPortListenerElement2 = document.getElementById('viewPortCheck_' + neededIDs[neededIDs.length - 1].id)
                        this.activateScrollListener(true);
                        this.showMessage("");
                    })
                })
            }


        } else {
            this.showMessage("Alle Angebote geladen!");
        }

    }

    showMessage(message) {
        this.statusFieldContainer.innerText = message;
    }

    loading() {
        $('#statusField').html("<img class='loader' src='loader.gif'>");
        // $('#statusField').html("<i class='fas fa-spinner fa-pulse'></i>");
    }

    checkIfReloadNecessary() {
        var inVP = isInViewport(this.inViewPortListenerElement1) || isInViewport(this.inViewPortListenerElement2);
        return inVP;
    }


}
