

class LoginController{
    constructor(){
        this.currentLoginRequest = null;
    }

    verifyUser(OnSuccess,OnFailure){
        if(this.isLoggedIn()){
            OnSuccess();
        }else{
            this.currentLoginRequest = new LoginRequest(this,OnSuccess,OnFailure);
        }

    }

    checkLoginStatusWithBackend(){
        application.backendInterface.getOwnUser((data)=>{
            if(data[0].id != null){
                application.currentUserID = data[0].id;
                this.changeLoggedInStatus(true)
            }else{
                this.changeLoggedInStatus(false)

            }
        })
    }

    isLoggedIn(){
        return window.localStorage.getItem('loggedIn') == 'true'
    }

    logout(){
        application.backendInterface.logoutUser((data)=>{
            
            this.changeLoggedInStatus(false)
            application.currentUserID = null;
            alert("Sie wurden ausgeloggt!");
            application.navigateTo('alleAngebote');
        })
    }

    changeLoggedInStatus(loggedIn){
        application.burgerMenuLeft.updateMenuPoints(loggedIn)
        window.localStorage.setItem('loggedIn',loggedIn)
    }

    sendLoginRequest(){
        let username = $('#loginEMAIL').val()
        let password = $('#loginPW').val()

        application.backendInterface.loginUser(username, password, (data)=>{
            if(data == 'E-Mail oder Passwort sind falsch.'){
                alert("Die Anmeldedaten sind nicht korrekt! Bitte prüfen Sie ihre Angaben.")
            }else if(data == 'Sie wurden erfolgreich eingeloggt.'){
                alert("Sie haben sich erfolgreich eingeloggt.");
                this.changeLoggedInStatus(true)
                this.currentLoginRequest.RequestSuccessfull();
            }else if(data == 'Bitte geben Sie eine E-Mail und ein Passwort an.'){
                alert("Bitte geben Sie ihre email-Adresse und ihr Passwort an")
            }else{
                alert("ERROR!")
            }
        })
    }

    showLoginScreen(){
        application.overlayHandler.showOverlay();
        application.overlayHandler.loadSimpleHTMLintoOverlay('Overlays/loginOverlay.html',()=>{
            this.initLoginScreen();
        });
    }

    initLoginScreen(){
        var selfLoginController = this;
        $('#loginToRegister').click(() => {
            application.overlayHandler.hideOverlay();
            application.navigateTo('legeUserAn')
        })

        $('#loginCloseButton').click(()=> {
            selfLoginController.currentLoginRequest.abortRequest();
        })

        $('#loginSend').click(()=>{
            this.sendLoginRequest();
        })
    }
}

class LoginRequest{
    constructor(loginController, OnSuccess,OnFailure){
        this.OnSuccess = OnSuccess;
        this.OnFailure = OnFailure;
        loginController.showLoginScreen();
    }

    abortRequest(){
        application.overlayHandler.hideOverlay()
        this.OnFailure();
    }

    RequestSuccessfull(){
        application.overlayHandler.hideOverlay();
        this.OnSuccess();
    }

}
