var postfach = ["postfach", "message", "Postfach"];
var home = ["alleAngebote", "rabbit", "Home"];
var login = ["login", "message", "Postfach"];
var profil = ["eigenesProfil", "profile", "Mein Profil"];
var logout = "<div class='burgerMenuPoint' onmousedown=\"application.loginController.logout()\">\n" +
    "                    <img src=\"icons/Profil.svg\">\n" +
    "                    <p>Logout</p>\n" +
    "            </div>"
class MenuController{
    eingeloggt(){
        console.log(this.createBurgerMenuPoint('eigeneAngebote', 'platzhalterIcon', 'Home'));
        $('.burgerMenuPointWrapper').empty();
        $('.burgerMenuPointWrapper').append(this.createBurgerMenuPoint(home) + this.createBurgerMenuPoint(postfach) + this.createBurgerMenuPoint(profil));
        $('.burgerMenuPointWrapper').append(logout)
    }
    ausgeloggt(){
        $('.burgerMenuPointWrapper').empty();
        $('.burgerMenuPointWrapper').append(this.createBurgerMenuPoint(home) + this.createBurgerMenuPoint(login));
    }
    createBurgerMenuPoint(menupunkt){
        var point = "<div class='burgerMenuPoint' onmousedown=\'application.navigateTo('"+ menupunkt[0] +"')\'>\n" +
            "                <img src=\'icons/"+ menupunkt[1] +".svg\'>\n" +
            "                <p>" + menupunkt[2] + "</p>\n" +
            "            </div>"
        return point
    }

}