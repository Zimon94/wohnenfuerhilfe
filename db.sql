CREATE TABLE angebot_biete (
    id int auto_increment,
    titel text,
    beschreibung text,
    autor smallint,
    latitude decimal,
    longitude decimal,
    bilder text,
    head_bild text,
    plz smallint,
    ort text,
    adresse text,
    preis int,
    aufgaben text,
    arbeitsstunden smallint,
    von int,
    bis int,
    aktiv smallint,
    groesse smallint,
    wohnungsart text,
    mietart text,
    ausstattung text,
    lage text,
    sonstiges text,
    PRIMARY KEY (id)
);

CREATE TABLE angebot_suche (
    id int auto_increment,
    titel text,
    beschreibung text,
    autor smallint,
    lat decimal,
    long decimal,
    umkreis int,
    plz smallint,
    ort text,
    adresse text,
    preis int,
    faehigkeiten text,
    von int,
    bis int,
    aktiv smallint,
    wohnungsart text,
    mietart text,
    sonstiges text,
    PRIMARY KEY (id)
);

CREATE TABLE bewertung (
    id int auto_increment,
    titel text,
    beschreibung text,
    autor text,
    adressat text,
    sterne smallint,
    PRIMARY KEY (id)
);

CREATE TABLE user (
    id int auto_increment,
    name text,
    nachname text,
    geschlecht text,
    beschreibung text,
    mail text,
    bild text,
    PRIMARY KEY (id)
);


CREATE TABLE schaets (
    id int auto_increment,
    user1 smallint,
    user2 smallint,
    PRIMARY KEY (id)
);


CREATE TABLE nachricht (
    id int auto_increment,
    id_schaet int,
    user int,
    nachricht text,
    datum smallint,
    PRIMARY KEY (id)
);



INSERT INTO
angebot_biete (titel,beschreibung,autor,bilder,head_bild,plz,ort,adresse,preis,aufgaben,arbeitsstunden,von,bis,aktiv,groesse,wohnungsart,mietart,ausstattung,lage,sonstiges)
 VALUES
 ('titel','beschreibung','11','bild','bildname','73202','mosbach','lettenbuehl','50','GartenarbeitBabysitten','10','10','10','1','20','WG','unbefristet','VieleMoebel','GuteLage','CoolesInserat')


 INSERT INTO
 angebot_biete (titel,beschreibung,autor,bilder,head_bild,plz,ort,adresse,preis,aufgaben,arbeitsstunden,von,bis,aktiv,groesse,wohnungsart,mietart,ausstattung,lage,sonstiges)
 VALUES
 ('titel','beschreibung','11,bild','bildname','73202','mosbach','lettenbuehl 35','50','GartenarbeitBabysitten','10','10102018','1012019','1','20','WG','unbefristet','Viele Moebel','Gute Lage','Cooles Inserat')
