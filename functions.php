<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// functions to manage the offers of WFH


function getOffer($array,$type){
  global $pdo;
  $table = "";
  $where = " WHERE 1=1";
  $ajax = "*";
    if($type == "house"){
      $table = "angebot_biete";
    }
    if($type == "help"){
      $table = "angebot_suche";
    }
      if(isset($array['ids'])){
        $i = 1;
        foreach ($array['ids'] as $key => $value) {
        if($i == 1){
          $where .= " AND id = ".$value;
        }
        else{
          $where .= " OR id = ".$value;
        }
        $i++;}}
      if(isset($array['filter'])){
        if(isset($array['filter']['autor']) && $array['filter']['autor'] != ""){
          $where = " WHERE `autor` = '".$array['filter']['autor']."'";
          $ajax = "id";
        }
        else{
        if($array['filter']['all'] == "1"){
          $where = "";
          if(isset($array['filter']['ajax']) && $array['filter']['ajax']== "1"){
            $ajax = "id";
          }
        }
        else{
        if(isset($array['filter']['ort']) && $array['filter']['ort'] != ""){
          $where .= " AND `ort` = '".$array['filter']['ort']."'";
        }
        if(isset($array['filter']['umkreis']) && $array['filter']['umkreis'] != "0"){
          $where .= @getDistanceIds($array['filter']['umkreis'],$array['filter']['ort'],$table);
        }
        if(isset($array['filter']['max_miete']) && $array['filter']['max_miete'] != ""){
          $where .= " AND `preis` < '".$array['filter']['max_miete']."'";
        }
        if(isset($array['filter']['min_miete']) && $array['filter']['min_miete'] != ""){
          $where .= " AND `preis` > '".$array['filter']['min_miete']."'";
        }
        if(isset($array['filter']['mietart']) && $array['filter']['mietart'] != ""){
          $where .= " AND `mietart` = '".$array['filter']['mietart']."'";
        }
        if(isset($array['filter']['aufgabenfeld']) && $array['filter']['aufgabenfeld'] != ""){
          $i="0";
          foreach ($array['filter']['aufgabenfeld'] as $key => $value) {
            $where .= " AND `aufgaben` LIKE '%".$array['filter']['aufgabenfeld'][$i]."%'";
            $i++;
          }
        }
      }}}
        $sql = "SELECT ".$ajax." FROM ".$table.$where;
        // echo $sql;
        $query = $pdo->query($sql);
        $result = $query->setFetchMode(PDO::FETCH_ASSOC);
        $data = [];
        while ($row = $query->fetch()) {
          if($ajax == "*"){
            $user_sql = "SELECT `id`,`name`,`nachname`,`bild`,`geschlecht`,`beschreibung`,`mail` FROM user WHERE `id` = '{$row['autor']}'";
            $user_query = $pdo->query($user_sql);
            $user_result = $user_query->setFetchMode(PDO::FETCH_ASSOC);
            $user_row = $user_query->fetch();
            $row['user'] = $user_row;


            if($table == "angebot_biete"){
         $row['bilder'] = array_filter(explode("//",$row['bilder']));
         $row['ausstattung'] = array_filter(explode("//",$row['ausstattung']));
       }
          $row['aufgaben'] = array_filter(explode("//",$row['aufgaben']));
        }
        $data[] =$row;
      }
      if(isset($array['filter']['autor']) && $array['filter']['autor'] != ""){
        return $data;
      }
      else{
      header('Content-Type: application/json');
      echo json_encode($data);
}
}




function editOffer($array,$type){
  if(isset($_SESSION['userid'])){
global $pdo;
$table = "";
$sqlColumn ="";
$sqlValues ="";
$sql="";
$bilder="";
$aufgaben="";
$userID = $_SESSION['userid'];
foreach ($array['aufgabenfeld'] as $key => $value) {
$aufgaben .=$value;
}

if(isset($array['id']) && !empty($array['id'])){
if($type == "house"){
  $table = "angebot_biete";
  $allowed = correctUser($table,$array['id']);
  foreach ($array['bilder'] as $key => $value) {
  $bilder .=$value."//";
  }
  $sql="UPDATE ".$table." SET `titel` = '{$array['titel']}', `beschreibung` = '{$array['beschreibung']}', `autor` = '{$userID}', `bilder`= '{$bilder}', `head_bild` = '{$array['hauptbild']}', `plz` = '{$array['plz']}', `ort` = '{$array['ort']}', `adresse` = '{$array['adresse']}', `preis` = '{$array['preis']}', `aufgaben` = '{$aufgaben}',
   `arbeitsstunden` = '{$array['arbeitsstunden']}', `von` = '{$array['von']}', `bis` =  '{$array['bis']}', `aktiv` = '{$array['aktiv']}', `groesse` = '{$array['groesse']}', `wohnungsart` = '{$array['wohnungsart']}',
    `mietart` = '{$array['mietart']}', `ausstattung` = '{$array['ausstattung']}', `lage` = '{$array['lage']}', `sonstiges` = '{$array['sonstiges']}' WHERE `id` = '{$array['id']}'";
}
if($type == "help"){
  $table = "angebot_suche";
  $allowed = correctUser($table,$array['id']);
  $sql="UPDATE ".$table." SET `titel` = '{$array['titel']}', `beschreibung` = '{$array['beschreibung']}', `autor` = '{$userID}', `umkreis` = '{$array['umkreis']}', `plz` = '{$array['plz']}', `ort` = '{$array['ort']}', `adresse` = '{$array['adresse']}', `preis` = '{$array['preis']}', `aufgaben` = '{$aufgaben}',
   `von` = '{$array['von']}', `bis` =  '{$array['bis']}', `aktiv` = '{$array['aktiv']}', `wohnungsart` = '{$array['wohnungsart']}',
    `mietart` = '{$array['mietart']}',`sonstiges` = '{$array['sonstiges']}',`arbeitsstunden`= '{$array['arbeitsstunden']}' WHERE `id` = '{$array['id']}'";
}
  if($allowed){
    $succes=$pdo->exec($sql);
    if($succes){echo "Ihr Angebot wurde erfolgreich geändert.";}
    else{echo "Es ist ein Problem aufgetreten.";}
  }
}
else{echo "Bitte geben Sie eine gültige ID an.";}
}
else{
  echo "Sie müssen eingelogt sein um ein Angebot zu bearbeiten.";
}}



function createOffer($array,$type){
  if(isset($_SESSION['userid']))
  {
global $pdo;
$table = "";
$sqlColumn ="";
$sqlValues ="";
$sql="";
$bilder="";
$aufgaben="";
$ausstattung="";
$userID = $_SESSION['userid'];

foreach ($array['aufgabenfeld'] as $key => $value) {
$aufgaben .=$value."//";
}

if($type == "house"){
  $table = "angebot_biete";
  foreach ($array['bilder'] as $key => $value) {
  $bilder .=$value."//";
  }
  foreach ($array['ausstattung'] as $key => $value) {
  $ausstattung .=$value."//";
  }
  $sql="INSERT INTO ".$table." (titel,beschreibung,autor,bilder,head_bild,plz,ort,adresse,preis,aufgaben,arbeitsstunden,von,bis,aktiv,groesse,wohnungsart,mietart,ausstattung,lage,sonstiges) VALUES
  ('{$array['titel']}','{$array['beschreibung']}','{$userID}','{$bilder}','{$array['hauptbild']}','{$array['plz']}','{$array['ort']}','{$array['adresse']}','{$array['preis']}','{$aufgaben}','{$array['arbeitsstunden']}','{$array['von']}',
    '{$array['bis']}','{$array['aktiv']}','{$array['groesse']}','{$array['wohnungsart']}','{$array['mietart']}','{$ausstattung}','{$array['lage']}','{$array['sonstiges']}')";
}
if($type == "help"){
  $table = "angebot_suche";
  $sql="INSERT INTO ".$table." (titel,beschreibung,autor,umkreis,plz,ort,adresse,preis,aufgaben,von,bis,aktiv,wohnungsart,mietart,sonstiges,arbeitsstunden) VALUES
  ('{$array['titel']}','{$array['beschreibung']}','{$userID}','{$array['umkreis']}','{$array['plz']}','{$array['ort']}','{$array['adresse']}','{$array['preis']}','{$aufgaben}','{$array['von']}',
    '{$array['bis']}','{$array['aktiv']}','{$array['wohnungsart']}','{$array['mietart']}','{$array['sonstiges']}','{$array['arbeitsstunden']}')";
}
    $succes=$pdo->exec($sql);
    if($succes){echo "Ihr Angebot wurde erstellt.";}
    else{echo "Es ist ein Problem aufgetreten.";}
}
else{echo "Sie müssen eingelogt sein um ein Angebot erstellen zu können.";}
}



function deleteOffer($array,$type){
  global $pdo;
  if($type == "house"){
    $table = "angebot_biete";
  }
  if($type == "help"){
    $table = "angebot_suche";
  }
  if(isset($array['id']) && !empty($array['id'])){
    $allowed = correctUser($table,$array['id']);
    if($allowed){
  $sql = "DELETE FROM ".$table." WHERE `id` = '".$array['id']."'";
  $succes=$pdo->exec($sql);
  if($succes)
  {echo "Das Angebot wurde gelöscht.";}
  else
  {echo "Es ist ein Problem aufgetreten.";}
}}
else{echo "Bitte geben Sie eine ID an um ein Angebot zu löschen.";}
}




// functions to manage the users of WFH

function getUser($array){
  global $pdo;
  $selfUser = false;
  if(isset($array['self']) && $array['self'] == "1" && !isset($_SESSION['userid'])){
    echo "Sie sind nicht eingeloggt.";
  }
  else{
  if(isset($array['id']) && $array['id'] != ""){
    $id = $array['id'];
  }
  if(isset($array['self']) && $array['self'] == "1" && isset($_SESSION['userid'])){
    $selfUser = true;
    $id = $_SESSION['userid'];
    $filterArray = [];
    $filterArray['filter']['autor'] = $id;
    $biete_data = getOffer($filterArray,"house");
    $suche_data = getOffer($filterArray,"help");
    }
  }
    $sql = "SELECT `id`,`name`,`geschlecht`,`nachname`,`beschreibung`,`mail`,`bild` FROM user WHERE `id` = '{$id}'";
    // echo $sql;
    $query = $pdo->query($sql);
    $result = $query->setFetchMode(PDO::FETCH_ASSOC);
    $row = $query->fetchAll();
    if(empty($row)){
      echo "Zu dieser ID gibt es keinen Nutzer.";
    }
    else{
      if($selfUser){
        $row['0']['angebot_biete'] = $biete_data;
        $row['0']['angebot_suche'] = $suche_data;
      }
      header('Content-Type: application/json');
      echo json_encode($row);
    }
  }



function editUser($array){
    if(isset($_SESSION['userid'])) {
  global $pdo;
  $table = "user";
  $addsql ="";
  $mailExists = false;
  $sql = "SELECT `mail`,`id` FROM user";
  // echo $sql;
  $query = $pdo->query($sql);
  $result = $query->setFetchMode(PDO::FETCH_ASSOC);
  while ($row = $query->fetch()) {
    if($row['mail'] == $array['mail'] && $row['id'] != $_SESSION['userid']){
      echo "Die E-Mail Adresse wird bereits verwendet.";
      $mailExists = true;
      break;
    }
  }
  if(!$mailExists){
    if(isset($array['passwort'])){
      $passwort = password_hash($array['passwort'],PASSWORD_BCRYPT);
      $addsql = ",`passwort` = '{$passwort}'";
    }

    $sql="UPDATE ".$table." SET `name` = '{$array['name']}', `nachname` = '{$array['nachname']}', `geschlecht` = '{$array['geschlecht']}', `beschreibung` = '{$array['beschreibung']}', `mail` = '{$array['mail']}', `bild` = '{$array['bild']}'".$addsql." WHERE `id` = '{$_SESSION['userid']}'";
    echo $sql;
    $succes=$pdo->exec($sql);
    if($succes){echo "Nutzer wurde bearbeitet.";}
    else{echo "Es ist ein Problem aufgetreten.";}
  }
  }
  else{echo "Sie müssen zuerst eingeloggt sein um Änderungen vorzunehmen.";}
}

function createUser($array){
  global $pdo;
  $table = "user";
  $token = randHash();
  $passwort = password_hash($array['passwort'],PASSWORD_BCRYPT);
  $sql="INSERT INTO ".$table." (name,nachname,geschlecht,beschreibung,mail,bild,passwort,hash,aktiv) VALUES
  ('{$array['name']}','{$array['nachname']}','{$array['geschlecht']}','{$array['beschreibung']}','{$array['mail']}','{$array['bild']}','{$passwort}','{$token}','0')";
  $succes=$pdo->exec($sql);
  $mailSucces = sendMail($array['mail'],$token);
  if($succes && $mailSucces){echo "Nutzer wurde angelegt.";}
  else{echo "Es ist ein Problem aufgetreten.";}
}


function deleteUser($array){
  global $pdo;
  if(isset($_SESSION['userid'])){
    $sql = "DELETE FROM user WHERE `id` = '{$_SESSION['userid']}'";
    echo $sql;
    $succes=$pdo->exec($sql);
    if($succes){
      echo "Der Nutzer wurde gelöscht.";
      unset($_SESSION['userid']);
    }
    else{
      echo "Es ist ein Problem aufgetreten.";
    }
  }
  else{
    echo "Sie sind nicht eingeloggt.";
  }
}

function loginUser($array){
  global $pdo;
  if(isset($array['mail']) && $array['mail'] != ""  && isset($array['passwort']) && $array['passwort'] != "") {

    $mail = $array['mail'];
    $passwort = $array['passwort'];

    $statement = $pdo->prepare("SELECT * FROM user WHERE mail = :mail");
    $result = $statement->execute(array('mail' => $mail));
    $user = $statement->fetch();

    //Überprüfung des Passworts
    if ($user !== false && password_verify($passwort, $user['passwort'])) {
        $_SESSION['userid'] = $user['id'];
        echo "Sie wurden erfolgreich eingeloggt.";

    } else {
        echo "E-Mail oder Passwort sind falsch.";
    }
}
else {
  echo "Bitte geben Sie eine E-Mail und ein Passwort an.";
}
}
function logoutUser($array){
  if(isset($array['logout'])){
    session_destroy();
    echo "Sie wurden ausgelogt.";
  }
}


function correctUser($table,$id){
  global $pdo;
  $sql = "SELECT * FROM $table WHERE `id`= '{$id}'";
  // echo $sql;
  $query = $pdo->query($sql);
  $result = $query->setFetchMode(PDO::FETCH_ASSOC);
  $row = $query->fetch();
  if(isset($_SESSION['userid'])){
  if($row['autor'] == $_SESSION['userid']){
  return true;
}
else{
  echo "Sie können nur ihre eigenen Inhalte editieren.";
  return false;
}}
else{
  echo "Sie müssen eingelogt sein um diese Aktion durchzuführen.";
  return false;
}
}



function sendMail($mail,$token){
  require 'phpmailer/Exception.php';
  require 'phpmailer/PHPMailer.php';
  require 'phpmailer/SMTP.php';
  $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
  //Enable SMTP debugging.
  $mail->SMTPDebug = 0;
  //Set PHPMailer to use SMTP.
  $mail->isSMTP();
  //Set SMTP host name
  $mail->Host = "smtp.gmail.com";
  //Set this to true if SMTP host requires authentication to send email
  $mail->SMTPAuth = true;
  //Provide username and password
  $mail->Username = "simon.schaeffer94@googlemail.com";
  $mail->Password = "ZZ8090LOlo@";
  //If SMTP requires TLS encryption then set it
  //$mail->SMTPSecure = "tls";
  //Set TCP port to connect to
  $mail->Port = 587;
  $mail->From = "wohnenfuerhilfe@gmail.com";
  $mail->FromName = "WFH Team";
  $mail->CharSet = 'UTF-8';

  $mail->smtpConnect(
      array(
          "ssl" => array(
              "verify_peer" => false,
              "verify_peer_name" => false,
              "allow_self_signed" => true
          )
      )
  );
  $mail->addAddress("simon.schaeffer94@googlemail.com", "Recepient Name");
  $mail->isHTML(true);
  $mail->Subject = "Wohnen Für Hilfe - Registrierung";
  $mail->Body = "<p>Vielen Dank für Ihre Anmeldung bei Wohnen für Hilfe.<br>
                    Um Ihre Registrierung zu vollenden klicken Sie bitte auf folgeden Link:<br></p>
                    <a href='http://localhost/wfh/wohnenfuerhilfe/ajaxController.php?token=".$token."'>Registrierung vollenden</a>";
  $mail->AltBody = "This is the plain text version of the email content";
  if(!$mail->send())
  {
      // echo "Mailer Error: " . $mail->ErrorInfo;
      return false;
  }
  else
  {
      return true;
  }
}



function checkMail($array){
  global $pdo;
  $table = "user";
  $newMail = true;
  $sql = "SELECT `mail` FROM user";
  // echo $sql;
  $query = $pdo->query($sql);
  $result = $query->setFetchMode(PDO::FETCH_ASSOC);
  while ($row = $query->fetch()) {
    if($row['mail'] == $array['mail']){
      $newMail = false;
      break;
    }
}
header('Content-Type: application/json');
echo json_encode($newMail);
}


function validateToken($token){
  global $pdo;
  $table = "user";
  $newMail = true;
  $sql = "SELECT `hash`,`id`,`aktiv` FROM user WHERE `hash` = '{$token}'";
  $query = $pdo->query($sql);
  $result = $query->setFetchMode(PDO::FETCH_ASSOC);
  if($query->rowCount() > 0){
    $row = $query->fetch();
    if($row['aktiv'] == '0'){
      $sql = "UPDATE user SET `aktiv` = '1' WHERE `id` = '{$row['id']}'";
      $succes=$pdo->exec($sql);
      if($succes){echo "Ihr Konto wurde aktiviert.";}
      else{echo "Es ist ein Problem aufgetreten.";}
    }
    else{
      echo "Der Account wurde bereits aktiviert.";
    }
    }
    else{
      echo "Der Account existiert nicht.";
    }
}


function randHash($len=32){
	return substr(md5(openssl_random_pseudo_bytes(20)),-$len);
}



function getDistanceIds($distance,$location,$table){
  global $pdo;
  $ids="";
  $plzs="";
  $i=0;
  $i_inserts=0;
  $data = [];
  $sql = "SELECT `plz`,`id` FROM ".$table;
  // echo $sql;
  $query = $pdo->query($sql);
  $result = $query->setFetchMode(PDO::FETCH_ASSOC);
  while ($row = $query->fetch()) {
    $data[] = $row['id'];
    $plzs .= $row['plz'].",DE|";
}
$url = "https://maps.googleapis.com/maps/api/distancematrix/json?&origins=".$location."&destinations=".$plzs."&key=AIzaSyB0IqwDLCBoYlcNOVSSyAxb_a5BUfzLGa8";
// echo $url;
$array = json_decode(file_get_contents("https://maps.googleapis.com/maps/api/distancematrix/json?&origins=".$location."&destinations=".$plzs."&key=AIzaSyB0IqwDLCBoYlcNOVSSyAxb_a5BUfzLGa8"));
$distance_location = $array->rows[0]->elements;

foreach ($distance_location as $key => $value) {
$realDistance = $value->distance->value;
if($realDistance <= $distance){
if($i_inserts == 0){
$ids .= "OR `id` = '".$data[$i]."'";
}
else{
  $ids .= " OR `id` = '".$data[$i]."'";
}
$i_inserts++;
}
$i++;
}
return $ids;
}

function createChat($array){
  if(isset($_SESSION['userid'])){
  global $pdo;
  $response_array = [];
  $go_on = true;
  $sql = "SELECT * FROM chat";
  $query = $pdo->query($sql);
  $result = $query->setFetchMode(PDO::FETCH_ASSOC);
  if ($query->rowCount() > 0) {
    while ($row = $query->fetch()) {
  if(($array['user_id'] == $row['user1'] || $array['user_id'] == $row['user2']) && ($_SESSION['userid'] == $row['user1'] || $_SESSION['userid'] == $row['user2']) && ($row['angebot_id'] == $array['angebot_id'])){
    $response_array['chat_id'] = $row['id'];
    $response_array['info'] = "Es besteht bereits eine Schät mit dieser Person.";
    header('Content-Type: application/json');
    echo json_encode($response_array);
    $go_on = false;
    break;
      }
    }
  }
if($go_on){
  $sql="INSERT INTO chat (user1,user2,angebot_type,angebot_id,angebot_headline) VALUES
  ('{$_SESSION['userid']}','{$array['user_id']}','{$array['angebot_type']}','{$array['angebot_id']}','{$array['angebot_headline']}')";
  // echo $sql;
  $succes=$pdo->exec($sql);
  $lastId = $pdo->lastInsertId();
  if($succes){
    $response_array['chat_id'] = $lastId;
    $response_array['info'] = "Schät wurde erstellt.";
    header('Content-Type: application/json');
    echo json_encode($response_array);
  }
  else{
    echo "Es ist ein Fehler aufgetreten.";
  }
}
}
else{
  echo "Sie müssen eingeloggt sein um Nachrichten zu schreiben.";
}
}



function sendChat($array){
  global $pdo;
  $time = time();
  if(isset($_SESSION['userid'])){
  $sql = "SELECT * FROM chat WHERE `id` = '{$array['toChat']}'";
  $query = $pdo->query($sql);
  $result = $query->setFetchMode(PDO::FETCH_ASSOC);
  if ($query->rowCount() > 0) {
    $sql="INSERT INTO nachricht (chat_id,von,message,datum,gelesen) VALUES
    ('{$array['toChat']}','{$_SESSION['userid']}','{$array['message']}','{$time}','0')";
    $succes=$pdo->exec($sql);
    if($succes){
      echo "Nachricht wurde versendet.";
    }
    else{
      echo "Es ist ein Fehler aufgetreten.";
    }
  }
  else{
    echo "Einen Chat mit dieser ID gibt es nicht.";
  }
}
  else{
    echo "Sie müssen eingeloggt sein um Nachrichten zu schreiben.";
  }
}


function getChat($array){
  if(isset($_SESSION['userid'])){
    $get_messages = false;
    $order_limit ="";
    $i = 0;
    $postfach_array = [];
    $nachrichten_array = [];
    global $pdo;
    $sql = "SELECT * FROM chat";
    $where = " WHERE `user1` = '{$_SESSION['userid']}' OR `user2` = '{$_SESSION['userid']}'";
if(isset($array['id']) && $array['id'] != ''){
    $where = " WHERE `id` = '{$array['id']}' AND (`user1` = '{$_SESSION['userid']}' OR `user2` = '{$_SESSION['userid']}')";
    $get_messages = true;
}
  // echo $sql.$where;
    $query = $pdo->query($sql.$where);
    $result = $query->setFetchMode(PDO::FETCH_ASSOC);
    if ($query->rowCount() > 0) {
      while($chat_array = $query->fetch()){
      $user1_sql = "SELECT `id`,`name`,`geschlecht`,`nachname`,`beschreibung`,`mail`,`bild` FROM user WHERE `id` = '{$chat_array['user1']}'";
      $query1 = $pdo->query($user1_sql);
      $result = $query1->setFetchMode(PDO::FETCH_ASSOC);
      $user1_array = $query1->fetch();

      $user2_sql = "SELECT `id`,`name`,`geschlecht`,`nachname`,`beschreibung`,`mail`,`bild` FROM user WHERE `id` = '{$chat_array['user2']}'";
      $query2 = $pdo->query($user2_sql);
      $result = $query2->setFetchMode(PDO::FETCH_ASSOC);
      $user2_array = $query2->fetch();

      if(isset($array['all']) && $array['all'] == '1'){
        $order_limit = " ORDER BY `datum` DESC LIMIT 1";
      }
      else{
        $order_limit = " ORDER BY `datum` DESC";
      }
      $nachricht_sql = "SELECT * FROM nachricht WHERE `chat_id` = '{$chat_array['id']}' ";
      $query3 = $pdo->query($nachricht_sql." ".$order_limit);
      $result = $query3->setFetchMode(PDO::FETCH_ASSOC);
      $ii = 0;
      while($row = $query3->fetch()){
        if($row['von'] == $_SESSION['userid']){
          $von = "self";
        }
        else{
          $von = "other";
        }
        $row['von'] = $von;
        $nachrichten_array[$ii] = $row;
        $ii++;
      }
      if($user1_array['id'] == $_SESSION['userid']){
        $user1_name = "self";
        $user2_name = "other";
      }
      else{
        $user1_name = "other";
        $user2_name = "self";
      }
      $postfach_array[$i][$user1_name] = $user1_array;
      $postfach_array[$i][$user2_name] = $user2_array;
      $postfach_array[$i]['chat'] = $chat_array;
      $postfach_array[$i]['messages'] = $nachrichten_array;
      if($get_messages){
        $last_message = current($nachrichten_array);
        if($last_message['von'] == "other"){
          $sql = "UPDATE nachricht SET `gelesen` = '1' WHERE `chat_id` = '{$last_message['chat_id']}'";
          $succes=$pdo->exec($sql);
        }
      }
      $nachrichten_array = [];
      $i++;
}
header('Content-Type: application/json');
echo json_encode($postfach_array);
    }
    else{
      echo "Es wurden keine Schäts gefunden.";
    }
}
else{
  echo "Sie müssen eingeloggt sein um Nachrichten anzusehen.";
}

}











 ?>
