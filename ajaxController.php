<?php
require_once("dbconnect.php");
require_once("functions.php");


session_start();

//print_r($_POST);
// data: { 'request':{'offer':{'house' :{'get' :{'filter':{'all': 0,'ort':'mosbach'}}}}}},


if(isset($_GET['token']) && !empty($_GET['token'])){
  validateToken($_GET['token']);
}


if(isset($_POST['request']) && !empty($_POST['request'])){
  $request_array = $_POST['request'];
  handleRequest($request_array);
}


function handleRequest($request_array){

  if(isset($request_array['offer'])){
    $request_array = $request_array['offer'];
    $type = "offer";
  }
  if(isset($request_array['user'])){
    $request_array = $request_array['user'];
    $type = "user";
  }
  if(isset($request_array['chat'])){
    $request_array = $request_array['chat'];
    $type = "chat";
  }
  if(isset($type) && $type == "offer")
  {
    if(isset($request_array['house'])){
      $request_array = $request_array['house'];
      $offerType = "house";
    }
      elseif(isset($request_array['help'])){
      $request_array = $request_array['help'];
      $offerType = "help";
      }

      if(isset($request_array['get'])){
        getOffer($request_array['get'],$offerType);
      }
      if(isset($request_array['edit'])){
        editOffer($request_array['edit'],$offerType);
      }
      if(isset($request_array['create'])){
        createOffer($request_array['create'],$offerType);
      }
      if(isset($request_array['delete'])){
        deleteOffer($request_array['delete'],$offerType);
      }
    }
    if(isset($type) && $type == "user"){
      if(isset($request_array['get'])){
        getUser($request_array['get']);
      }
      if(isset($request_array['edit'])){
        editUser($request_array['edit']);
      }
      if(isset($request_array['create'])){
        createUser($request_array['create']);
      }
      if(isset($request_array['delete'])){
        deleteUser($request_array['delete']);
      }
      if(isset($request_array['login'])){
        loginUser($request_array['login']);
      }
      if(isset($request_array['logout'])){
        logoutUser($request_array['logout']);
      }
      if(isset($request_array['checkmail'])){
        checkMail($request_array['checkmail']);
      }
    }

    if(isset($type) && $type == "chat"){
      if(isset($request_array['get'])){
        getChat($request_array['get']);
      }
      if(isset($request_array['send'])){
        sendChat($request_array['send']);
      }
      if(isset($request_array['create'])){
        createChat($request_array['create']);
      }
    }

}
 ?>
