'use strict';

var cacheVersion = 123;
var currentCache = {
    offline: 'offline' + cacheVersion
};
const offlineUrl = 'offline.html';

this.addEventListener('install', event => {
    event.waitUntil(
        caches.open(currentCache.offline).then(function(cache) {
            return cache.addAll([
                offlineUrl,
                'service-worker.js',
                'icon.svg',
                'manifest.json',
                'css/header.css',
                'css/burgerMenu.css',
                'css/main.css',
                'css/uebersicht.css',
                'https://cdn.jsdelivr.net/npm/animate.css@3.5.2/animate.min.css',
                'https://fonts.googleapis.com/css?family=Lato',
                'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',
                'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'
            ]);
        })
    );
});

self.addEventListener('fetch', function(event) {
    // Only fall back for HTML documents.
    var request = event.request;
    console.log(request);
    // && request.headers.get('accept').includes('text/html')
    if (request.method === 'GET') {
        // `fetch()` will use the cache when possible, to this examples
        // depends on cache-busting URL parameter to avoid the cache.
        event.respondWith(
            fetch(request).catch(function(error) {
                console.error(
                    '[onfetch] Failed. Serving cached offline fallback ' +
                    error
                );
                return caches.open('offline'+cacheVersion).then(function(cache) {
                    return cache.match(offlineUrl);
                });
            })
        );
    }
});


